#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
library(rBDAT)

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel("running BDAT on your inventory data"),

    fillPage(
        mainPanel(
           fileInput('target_upload', 'Choose file to upload',
                     accept = c(
                         'text/csv',
                         'text/comma-separated-values',
                         '.csv'
                     )),
           radioButtons("colseparator","Column separator: ",choices = c(";",",",":"), selected=";",inline=TRUE),
           radioButtons("decseparator","Decimal separator: ",choices = c(".",","), selected=",",inline=TRUE),
           radioButtons("yhat","what to be calculated: ", choices = c("Vfm","Efm", "assortment"), selected="Vfm",inline=TRUE),
           conditionalPanel(condition = "input.yhat == 'assortment'", 
                            sliderInput("GrDm", "Grenzdurchmesser", min=0, max=10, step=1, value=7)),
           plotOutput("distPlot"),
           downloadButton("downloadData", "Download")
        )
    )
)

# Define server logic required to draw a histogram
server <- function(input, output) {

    data_upload <- reactive({
        inFile <- input$target_upload
        if (is.null(inFile))
            return(NULL)
        df <- read.csv(inFile$datapath, header = TRUE, sep = input$colseparator)
        if(!is.null(df)){
          if(identical(input$yhat, "Vfm")){
            # Vfm
            df$Vfm <- rBDAT::getVolume(df)
          } else if(identical(input$yhat, "Efm")){
            # Efm
            df$A=0.01*df$H
            df$B=7
            df$Efm <- rBDAT::getVolume(df, iAB=c("H", "Dob"), bark=FALSE, )
          } else if(identical(input$yhat, "assortment")){
            tmp <- rBDAT::getAssortment(df, sort = list(Az=input$GrDm))
            if(!"tree" %in% colnames(df)) df$tree <- 1:nrow(df)
            df <- merge(df, tmp, by = "tree")
          }
        }
        return(df)
    })
    
    output$distPlot <- renderPlot({
        # generate bins based on input$bins from ui.R
        df <- data_upload()
        if(!is.null(df)){
          if(identical(input$yhat, "assortment")){
            plot(buildTree(df[1,]), assort = rBDAT::getAssortment(df[1,], sort = list(Az=input$GrDm)))
          } else {
            hist(df[, input$yhat], main=input$yhat, freq=TRUE)
          }
        }
    })
    
    # Downloadable csv of selected dataset ----
    output$downloadData <- downloadHandler(
        filename = function(){
          flnm <- input$target_upload$name
          flnm <- strsplit(flnm, split="\\.")[[1]][1]
          paste0(flnm, "_extended.csv")
        },
        content = function(file) {
          write.table(data_upload(), file, row.names = FALSE, sep = input$colseparator, dec= input$decseparator)
        }
    )
}

# Run the application 
shinyApp(ui = ui, server = server)
